#ifndef PXUTILSSTR_H_INCLUDED
#define PXUTILSSTR_H_INCLUDED

#include <string>

using namespace std;

string toNameWithUnits(const string &name, const string &units);


#endif // PXUTILSSTR_H_INCLUDED
