#ifndef PXPARAMDEFS_H_INCLUDED
#define PXPARAMDEFS_H_INCLUDED

#include <string>
#include "pxMacro.h"

using namespace std;

//====================      Common  =======================

typedef int PxUID;
typedef int PxID;

//====================      Convertors  =======================

class PxValueConvertor
{
public:
    //  Access to scales for GUI
    virtual int     getScaleCount() = 0;
    virtual string  getScaleName(const int & index) = 0;
    virtual double  getScaleValue(const int & index) = 0;
    virtual void    setScaleValue(const int & index, const double value) = 0;

    //  Changed
    //bool getChanged();
    //void clearChanged();

    //  Convert Funcs
    virtual bool toBaseValue(const double calibrValue, double & baseValue) = 0;
    virtual bool fromBaseValue(const double baseValue, double & calibrValue) = 0;

    //  Storage Control
    //function  LoadFromStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions): Boolean; virtual; abstract;
    //procedure SaveToStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions); virtual; abstract;

    //  Copy deleted
	//PxValueConvertor(const PxValueConvertor&) = delete;
	//PxValueConvertor& operator=(const PxValueConvertor&) = delete;
};

//=====================   Value Calibration ======================
//  - Defines - Type, Range and Step of Value
//  - Provides conversion to ParentValue.
//  - ValuesStr converts to Value as Index

enum class PxValueType {valInt, valDouble, valBool, valStr};

struct PxValueCfg
{
    PxValueType type;

    double  minValue;
    double  maxValue;
    double  step;
    int     prec;
    string  strValues;
};

class  PxValueCalibr
{
public:
    //  Calibr Info
    PxID getUnitsID();
    string getUnitsName();

    PxValueCfg getValueInfo();
    void setValueInfo(const PxValueCfg valueCfg);

    //  Value Convertions
    bool toBaseValue(double unitValue, double & baseValue);
    bool fromBaseValue(double baseValue, double & unitValue);

    //  ValueStr Convertions
    bool valueStrToIndex(string valueStr, int & ind);
    bool valueStrFromIndex(int ind, string & valueStr);

    //  Convertor - access for rescale
    PxValueConvertor* getConvertor();

    //  Changed
//    function  GetChanged: Boolean; virtual; abstract;
    //procedure ClearChanged; virtual; abstract;


    //  Storage Control
//    function  LoadFromStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions): Boolean; virtual; abstract;
//    procedure SaveToStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions); virtual; abstract;

//protected:

};



//====================      Parameters  =======================
enum class PxSetFlags
{
    flDef            = 0,
    flModifyOnly     = (1 << 0),
    flWaitCompleted  = (1 << 1)
};
DECLARE_ENUM_OPERATIONS(PxSetFlags)

enum class PxGetFlags
{
    flDef            = 0,
    flUpdate         = (1 << 0),
    flModifyOnUpdate = (1 << 1)
};
DECLARE_ENUM_OPERATIONS(PxGetFlags)


//  Executor - Applies param Value to Hardware or some targets, and Update also
class PxParam;

class PxParamExec
{
public:
    //  Validate and Return value, and then Apply it to target, return if applied.
    bool validateAndApplyValue(PxParam & param, double & value, const PxSetFlags &flags);
    //  Update and Validate value, return if value was changed
    bool validateAndUpdateValue(PxParam & param, double & value, const PxGetFlags &flags);

    bool applyAllParams();
};



//  It is just a unified storage for some value with calibrations.
//  Notify when changing, and accept notification from other params OnChange.
class PxParam
{
public:
    PxParam(const string & name, const string & showName,
            PxUID uid, PxID paramID, double value, PxParamExec * pExec,
            bool enabled = true) :
              fName(name), fShowName(showName),
              fUID(uid), fParamID(paramID), fValue(value), fpExec(pExec), fEnabled(enabled) {};

    //  Info
    string getName();
    PxUID  getUID();
    PxID   getParamID();

    //  ShowName
    void   setShowName(const string & showName);
    string getShowName(bool withUnits);

    //  Enabled
    void setEnabled(bool enabled);
    bool getEnabled();

    //  standard function with calls to ParamExec
    bool   setValue(double value, PxSetFlags flags = PxSetFlags::flDef);
    double getValue(PxGetFlags flags = PxGetFlags::flDef);
    bool   apply();   //  Apply active Value

    //  for value as string
    bool   setValueStr(const string & valueStr);
    bool   getValueStr(string & valueStr);

    //  for convenient
    bool   getValueBool();
    int    getValueInt();
    string getValueStr();


private:
    string fName;
    string fShowName;
    PxUID  fUID;
    PxID   fParamID;

    double fValue;
    PxParamExec* fpExec;

    bool fEnabled;

    void   setValueLoc(const double &value);
    double ensureValue(const double &value);
};


#endif // PXPARAMDEFS_H_INCLUDED
