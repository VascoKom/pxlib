#include "PxParamDefs.h"
#include "PxUtils.h"
#include "PxUtilsStr.h"

//  **********      PxParamExec - blank implementation  *********

bool PxParamExec::validateAndApplyValue(PxParam & param, double & value, const PxSetFlags &flags)
{
    return true;    // Applied Ok
}

bool PxParamExec::validateAndUpdateValue(PxParam & param, double & value, const PxGetFlags &flags)
{
    return false; // not changed
}

bool PxParamExec::applyAllParams()
{
    return true;
}

//  **********      PxParam - base implementation  *********

/*
PxParam::PxParam(const string & name, const string & showName,
            PxUID uid, PxID paramID, double value, const PxParamExec & exec) :
              fName(name), fShowName(showName),
              fUID(uid), fParamID(paramID), fValue(value), fExec(exec) {};
*/

//  Info
string PxParam::getName()
{
    return fName;
}

PxUID  PxParam::getUID()
{
    return fUID;
}

PxID   PxParam::getParamID()
{
    return fParamID;
}

//  ShowName
void   PxParam::setShowName(const string & showName)
{
    fShowName = showName;
}

string PxParam::getShowName(bool withUnits)
{
    if (withUnits)
        return toNameWithUnits(fShowName, "aa");
    else
        return fShowName;
}

//  Enabled
void PxParam::setEnabled(bool enabled)
{
    fEnabled = enabled;
}

bool PxParam::getEnabled()
{
    return fEnabled;
}

//  for value as string
bool   PxParam::setValueStr(const string & valueStr)
{

}

bool   PxParam::getValueStr(string & valueStr)
{

}

//  for convenient
bool   PxParam::getValueBool()
{
    return dblToBool(getValue());
}

int    PxParam::getValueInt()
{
    return dblToInt(getValue());
}

string PxParam::getValueStr()
{

}

//  standard function with calls to ParamExec
/*
double PxParam::getValue()
{
    double value{fValue};

    if (fpExec)
        if (fpExec->validateAndUpdateValue(*this, value, false))
            setValueLoc(value);

    return fValue;
}
*/

bool PxParam::setValue(double value, PxSetFlags flags)
{
    if (fEnabled)
    {
        bool result{true};

        double newValue = ensureValue(value);
        if ( (fpExec != nullptr)
            && ((PxSetFlags::flModifyOnly & flags) != PxSetFlags::flModifyOnly) )
                result = fpExec->validateAndApplyValue(*this, newValue, flags);

        if (result)
            setValueLoc(newValue);

        return result;
    }
    else
        return false;
}

double PxParam::getValue(PxGetFlags flags)
{
    double result = fValue;

    if (fpExec != nullptr)
        if (fpExec->validateAndUpdateValue(*this, result, flags))
            // value was updated
            if ((PxGetFlags::flModifyOnUpdate & flags) == PxGetFlags::flModifyOnUpdate)
                setValueLoc(result);

    return result;
}

bool PxParam::apply()
{
    return setValue(fValue);
}

//  Locals
void PxParam::setValueLoc(const double &value)
{
    fValue = value;
    //  Set changed
}

double PxParam::ensureValue(const double &value)
{

};
