#include <string>
#include "PxUtilsStr.h"

using namespace std;

string toNameWithUnits(const string &name, const string &units)
{
    if (units.empty())
        return name;
    else
        return name + ", " + units;
}


/*
void splitStr(const string& s, std::vector<string>& result, const string& delimiters = " ")
{
    string::size_type a, b = 0;

    for(;;)
    {    a = s.find_first_not_of(delimiters, b);
        b = s.find_first_of(delimiters, a);

        if (string::npos == b && string::npos == a) break;

        result.push_back(s.substr(a, b - a));
    }
}

*/
