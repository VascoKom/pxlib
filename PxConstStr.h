#ifndef PXCONSTSTR_H_INCLUDED
#define PXCONSTSTR_H_INCLUDED

#include <string>

using namespace std;

extern const string c_Scale;
extern const string c_Bias;

extern const string c_ErrName;


#endif // PXCONSTSTR_H_INCLUDED
