#ifndef PXUTILS_H_INCLUDED
#define PXUTILS_H_INCLUDED

//#include <string>

//using namespace std;

int  dblToInt(const double value);
bool dblToBool(const double value);

bool inRange(const int ind, const int cnt);

#endif // PXUTILS_H_INCLUDED
