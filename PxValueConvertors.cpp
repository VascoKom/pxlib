#include <cassert>
#include <iostream>

#include "PxValueConvertors.h"
#include "PxConstStr.h"
#include "PxUtils.h"

    //  Access to scales for GUI
enum PxScaleBiasID {idScale, idBias};

PxConvertorScaleBias::PxConvertorScaleBias(const double scale, const double bias)
{
    assert(scale != 0);

    fScales[idScale] = scale;
    fScales[idBias] = bias;
}

string  PxConvertorScaleBias::getScaleName(const int & index)
{
    switch (index)
    {
    case idScale:
        return c_Scale;
    case idBias:
        return c_Bias;
    default:
        assert(false);

        return c_ErrName;
    }
}

int     PxConvertorScaleBias::getScaleCount()
{
    return cScalesCount;
}

double  PxConvertorScaleBias::getScaleValue(const int & index)
{
    if (inRange(index, cScalesCount))
        return fScales[index];
    else
    {
        assert(false);

        cerr<<"getScaleValue: wrong index";
        return 1;
    }
}

void PxConvertorScaleBias::setScaleValue(const int & index, const double value)
{
    if (inRange(index, cScalesCount))
    {
        if (!(index == idScale) && (value == 0))
            fScales[index] = value;
        else
            cerr<<"PxConvertorScaleBias: Scale = 0";
    }

}

    //  Convert Funcs
bool PxConvertorScaleBias::toBaseValue(const double calibrValue, double & baseValue)
{
    try
    {
        baseValue = (calibrValue - fScales[idBias]) / fScales[idScale];
        return true;
    }
    catch (...)
    {
        return false;
    }
}

bool PxConvertorScaleBias::fromBaseValue(const double baseValue, double & calibrValue)
{
    calibrValue = baseValue * fScales[idScale] + fScales[idBias];
    return true;
}

    //  Changed
    //bool getChanged();
    //void clearChanged();

    //  Storage Control
    //function  LoadFromStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions): Boolean; virtual; abstract;
    //procedure SaveToStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions); virtual; abstract;

