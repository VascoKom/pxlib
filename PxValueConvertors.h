#ifndef PXVALUECONVERTORS_H_INCLUDED
#define PXVALUECONVERTORS_H_INCLUDED

#include "PxParamDefs.h"

class PxConvertorScaleBias: public PxValueConvertor
{
public:
    PxConvertorScaleBias(const double scale, const double bias);

    //  Access to scales for GUI
    int     getScaleCount() override;
    string  getScaleName(const int & index) override;
    virtual double  getScaleValue(const int & index) override;
    virtual void    setScaleValue(const int & index, const double value) override;

    //  Changed
    //bool getChanged();
    //void clearChanged();

    //  Convert Funcs
    bool toBaseValue(const double calibrValue, double & baseValue) override;
    bool fromBaseValue(const double baseValue, double & calibrValue) override;
private:
    static const int cScalesCount = 2;

    double fScales[cScalesCount];

    //  Storage Control
    //function  LoadFromStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions): Boolean; virtual; abstract;
    //procedure SaveToStorage(AStorage: IPxStorageSection; AOptions: TPxParamStorageOptions); virtual; abstract;
};

#endif // PXVALUECONVERTORS_H_INCLUDED
