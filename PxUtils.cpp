#include "PxUtils.h"

int dblToInt(const double value)
{
    return int(value);
}

bool dblToBool(const double value)
{
    return bool(int(value));
}

bool inRange(const int ind, const int cnt)
{
    return (ind >= 0) && (ind < cnt);
}
